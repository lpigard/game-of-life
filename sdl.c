#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_ttf.h>

#define TICK_INTERVAL    16
#define GAMMA 10
#define EPSILON 1

typedef struct particles
{
    double x;
    double y;
    double vx;
    double vy;
    double ax;
    double ay;
    double r;
    double m;
    SDL_Color c;
} Particle;

static Uint32 next_time;

Uint32 time_left(void)
{
    Uint32 now;

    now = SDL_GetTicks();
    if(next_time <= now)
    return 0;
    else
    return next_time - now;
}

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        printf("USAGE: ./SDL coloring_scheme [0, 1 or 2]\n");
        return -1;
    }

    int coloring_scheme = atoi(argv[1]);

    if(coloring_scheme != 0 && coloring_scheme != 1 && coloring_scheme != 2)
    {
        printf("Coloring_scheme = %d must be 0, 1 or 2.\n", coloring_scheme);
        return -1;
    }

    // returns zero on success else non-zero
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        printf("error initializing SDL: %s\n", SDL_GetError());
    }

    TTF_Init();

    srand(time(NULL));   // Initialization, should only be called once.

    SDL_DisplayMode DM;
    SDL_GetCurrentDisplayMode(0, &DM);
    int width = DM.w * 0.95;
    int height = DM.h * 0.95;

    SDL_Window* win = SDL_CreateWindow("GoL", // creates a window
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    width, height, 0);

    printf("Resolution = %d x %d (%d x %d)\n", width, height, DM.w, DM.h);

    // SDL_SetWindowFullscreen(win, SDL_WINDOW_FULLSCREEN);

    // triggers the program that controls
    // your graphics hardware and sets flags
    Uint32 render_flags = SDL_RENDERER_ACCELERATED;

    // creates a renderer to render our images
    SDL_Renderer* rend = SDL_CreateRenderer(win, -1, render_flags);

    TTF_Font* Sans = TTF_OpenFont("OpenSans-Regular.ttf", 24);
    SDL_Color Red = {255, 0, 0};

    // let us control our image position
    // so that we can move it with our keyboard.
    const int n_cells = height * width;
    int * cell = malloc(n_cells * sizeof *cell);
    int * new_cell = malloc(n_cells * sizeof *new_cell);

    for(int c = 0; c < n_cells; c++)
    {
        cell[c] = (rand() % 2) == 0;
    }

    // for(int c = 0; c < n_cells; c++)
    // {
    //     cell[c] = 0;
    // }
    //
    // int shift = width / 2 + width * height / 2;
    //
    // cell[shift + 1 + width * 0] = 1;
    // cell[shift + 2 + width * 1] = 1;
    // cell[shift + 0 + width * 2] = 1;
    // cell[shift + 1 + width * 2] = 1;
    // cell[shift + 2 + width * 2] = 1;

    int * neighbor_cell = malloc(8 * n_cells * sizeof *neighbor_cell);

    for(int x = 0; x < width; x++)
    {
        for(int y = 0; y < height; y++)
        {
            int n = 0;
            const int c = x + width * y;

            for(int dx = -1; dx <= 1; dx++)
            {
                for(int dy = -1; dy <= 1; dy++)
                {
                    if( dx != 0 || dy != 0)
                    {
                        int xn = x + dx;
                        if(xn < 0)
                        {
                            xn += width;
                        }
                        if(xn > width)
                        {
                            xn -= width;
                        }

                        int yn = y + dy;
                        if(yn < 0)
                        {
                            yn += height;
                        }
                        if(yn > height)
                        {
                            yn -= height;
                        }

                        int cn = xn + width * yn;
                        neighbor_cell[c * 8 + n] = cn;
                        n++;
                    }
                }
            }
        }
    }

    SDL_Texture * texture = SDL_CreateTexture(rend, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, width, height);
    // uint8_t * pixel;
    uint8_t * pixel = malloc(4 * n_cells * sizeof *pixel);
    int pitch;

    SDL_RendererInfo info;
    SDL_GetRendererInfo(rend, &info);

    printf("Renderer name: %s\n", info.name);
    printf("Texture formats: \n");
    for(Uint32 i = 0; i < info.num_texture_formats; i++ )
    {
        printf("%s\n", SDL_GetPixelFormatName(info.texture_formats[i]));
    }

    int close = 0;
    next_time = SDL_GetTicks() + TICK_INTERVAL;
    Uint32 past_time = SDL_GetTicks();

    // animation loop
    for(int t = 0; !close; t++)
    {
        SDL_Event event;
        // Events management
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                close = 1;
                break;

                case SDL_KEYDOWN:
                // keyboard API for key pressed
                switch (event.key.keysym.scancode)
                {
                    case SDL_SCANCODE_W:
                    case SDL_SCANCODE_UP:
                    case SDL_SCANCODE_D:
                    case SDL_SCANCODE_RIGHT:
                    coloring_scheme += 1;
                    if(coloring_scheme > 2)
                    {
                        coloring_scheme -= 3;
                    }
                    break;

                    case SDL_SCANCODE_S:
                    case SDL_SCANCODE_DOWN:
                    case SDL_SCANCODE_A:
                    case SDL_SCANCODE_LEFT:
                    coloring_scheme -= 1;
                    if(coloring_scheme < 0)
                    {
                        coloring_scheme += 3;
                    }
                    default:
                    break;
                }
            }
        }

        // clears the screen
        SDL_SetRenderDrawColor(rend, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(rend);

        // SDL_LockTexture(texture, NULL, (void**)&pixel, &pitch);

        switch(coloring_scheme)
        {
            case 0:
            for(int c = 0; c < n_cells; c++)
            {
                if(cell[c])
                {
                    pixel[c * 4 + 0] = 255;
                    pixel[c * 4 + 1] = 255;
                    pixel[c * 4 + 2] = 255;
                }
                else
                {
                    pixel[c * 4 + 0] = 0;
                    pixel[c * 4 + 1] = 0;
                    pixel[c * 4 + 2] = 0;
                }

                pixel[c * 4 + 3] = 255;
            }
            break;

            case 1:
            for(int c = 0; c < n_cells; c++)
            {
                int n_live_neighbors = 0;

                for(int n = 0; n < 8; n++)
                {
                    n_live_neighbors += cell[neighbor_cell[c * 8 + n]];
                }

                switch(n_live_neighbors)
                {
                    case 1:
                    pixel[c * 4 + 0] = 255;
                    pixel[c * 4 + 1] = 0;
                    pixel[c * 4 + 2] = 0;
                    break;
                    case 2:
                    pixel[c * 4 + 0] = 0;
                    pixel[c * 4 + 1] = 255;
                    pixel[c * 4 + 2] = 0;
                    break;
                    case 3:
                    pixel[c * 4 + 0] = 0;
                    pixel[c * 4 + 1] = 0;
                    pixel[c * 4 + 2] = 255;
                    break;
                    case 4:
                    pixel[c * 4 + 0] = 255;
                    pixel[c * 4 + 1] = 255;
                    pixel[c * 4 + 2] = 0;
                    break;
                    case 5:
                    pixel[c * 4 + 0] = 255;
                    pixel[c * 4 + 1] = 0;
                    pixel[c * 4 + 2] = 255;
                    break;
                    case 6:
                    pixel[c * 4 + 0] = 0;
                    pixel[c * 4 + 1] = 255;
                    pixel[c * 4 + 2] = 255;
                    break;
                    case 7:
                    pixel[c * 4 + 0] = 255;
                    pixel[c * 4 + 1] = 255;
                    pixel[c * 4 + 2] = 255;
                    break;
                    case 8:
                    pixel[c * 4 + 0] = 255;
                    pixel[c * 4 + 1] = 255;
                    pixel[c * 4 + 2] = 255;
                    break;
                    default:
                    pixel[c * 4 + 0] = 0;
                    pixel[c * 4 + 1] = 0;
                    pixel[c * 4 + 2] = 0;
                    break;
                }

                pixel[c * 4 + 3] = 255;
            }
            break;

            case 2:
            for(int c = 0; c < n_cells; c++)
            {
                if(cell[c])
                {
                    int n_live_neighbors = 0;

                    for(int n = 0; n < 8; n++)
                    {
                        n_live_neighbors += cell[neighbor_cell[c * 8 + n]];
                    }

                    switch(n_live_neighbors)
                    {
                        case 1:
                        pixel[c * 4 + 0] = 255;
                        pixel[c * 4 + 1] = 0;
                        pixel[c * 4 + 2] = 0;
                        break;
                        case 2:
                        pixel[c * 4 + 0] = 0;
                        pixel[c * 4 + 1] = 255;
                        pixel[c * 4 + 2] = 0;
                        break;
                        case 3:
                        pixel[c * 4 + 0] = 0;
                        pixel[c * 4 + 1] = 0;
                        pixel[c * 4 + 2] = 255;
                        break;
                        case 4:
                        pixel[c * 4 + 0] = 255;
                        pixel[c * 4 + 1] = 255;
                        pixel[c * 4 + 2] = 0;
                        break;
                        case 5:
                        pixel[c * 4 + 0] = 255;
                        pixel[c * 4 + 1] = 0;
                        pixel[c * 4 + 2] = 255;
                        break;
                        case 6:
                        pixel[c * 4 + 0] = 0;
                        pixel[c * 4 + 1] = 255;
                        pixel[c * 4 + 2] = 255;
                        break;
                        case 7:
                        pixel[c * 4 + 0] = 255;
                        pixel[c * 4 + 1] = 255;
                        pixel[c * 4 + 2] = 255;
                        break;
                        case 8:
                        pixel[c * 4 + 0] = 255;
                        pixel[c * 4 + 1] = 255;
                        pixel[c * 4 + 2] = 255;
                        break;
                        default:
                        pixel[c * 4 + 0] = 0;
                        pixel[c * 4 + 1] = 0;
                        pixel[c * 4 + 2] = 0;
                        break;
                    }
                }
                else
                {
                    pixel[c * 4 + 0] = 0;
                    pixel[c * 4 + 1] = 0;
                    pixel[c * 4 + 2] = 0;
                }

                pixel[c * 4 + 3] = 255;
            }
            break;
        }

        SDL_UpdateTexture(texture, NULL, pixel, width * 4);
        // SDL_UnlockTexture(texture);
        SDL_RenderCopy(rend, texture, NULL, NULL);

        for(int c = 0; c < n_cells; c++)
        {
            int n_live_neighbors = 0;

            for(int n = 0; n < 8; n++)
            {
                n_live_neighbors += cell[neighbor_cell[c * 8 + n]];
            }

            int new_cell_is_alive = cell[c] && (n_live_neighbors == 2 || n_live_neighbors == 3);
            new_cell_is_alive += !cell[c] && n_live_neighbors == 3;

            if(new_cell_is_alive)
            {
                new_cell[c] = 1;
            }
            else
            {
                new_cell[c] = 0;
            }
        }

        for(int c = 0; c < n_cells; c++)
        {
            cell[c] = new_cell[c];
        }

        char text[1024];
        Uint32 now = SDL_GetTicks();
        int fps = (int)nearbyint(1000 / (now - past_time));
        sprintf(text, "FPS = %d", fps);
        past_time = now;
        int w, h;
        TTF_SizeText(Sans, text, &w, &h);
        SDL_Surface* surfaceMessage = TTF_RenderText_Solid(Sans, text, Red);
        SDL_Texture* Message = SDL_CreateTextureFromSurface(rend, surfaceMessage);
        SDL_Rect Message_rect; //create a rect
        Message_rect.x = 0;  //controls the rect's x coordinate
        Message_rect.y = 0; // controls the rect's y coordinte
        Message_rect.w = w; // controls the width of the rect
        Message_rect.h = h; // controls the height of the rect

        SDL_RenderCopy(rend, Message, NULL, &Message_rect);
        SDL_FreeSurface(surfaceMessage);
        SDL_DestroyTexture(Message);

        // triggers the double buffers
        // for multiple rendering
        SDL_RenderPresent(rend);
        SDL_Delay(time_left());

        if(!t)
        {
            sleep(1);
        }
        next_time += TICK_INTERVAL;
    }

    free(pixel);
    free(cell);
    free(new_cell);
    free(neighbor_cell);

    // destroy renderer
    SDL_DestroyRenderer(rend);

    // destroy window
    SDL_DestroyWindow(win);

    TTF_Quit();

    // close SDL
    SDL_Quit();

    return 0;
}
